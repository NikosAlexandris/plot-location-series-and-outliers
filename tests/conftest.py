import os
import pathlib
import math
import netCDF4 as nc
import numpy as np
import pytest
from datetime import datetime, timedelta


# (longitude, latitude)
EU_GEOMETRIC_CENTER_POST_BREXIT = (9.902056, 49.843)
LONGITUDE_LOW=math.floor(EU_GEOMETRIC_CENTER_POST_BREXIT[0])
LONGITUDE_HIGH=math.ceil(EU_GEOMETRIC_CENTER_POST_BREXIT[0])
LATITUDE_LOW=math.floor(EU_GEOMETRIC_CENTER_POST_BREXIT[1])
LATITUDE_HIGH=math.ceil(EU_GEOMETRIC_CENTER_POST_BREXIT[1])


@pytest.fixture
def path_to_data(request):
    """
    Dynamically resolve and return the absolute path to 'tests/data'
    """
    module_directory = pathlib.Path(os.path.dirname(os.path.abspath(request.module.__file__)))
    return module_directory / 'data'


@pytest.fixture
def create_minimal_netcdf(path_to_data: pathlib.Path, time=24, lon=2, lat=2):
    
    np.random.seed(43)  # Fix the random seed to ensure reproducibility

    # Define the dimensions
    time = 24  # 24 hours
    lon = 2
    lat = 2

    # Create the netCDF file
    filename = path_to_data / "minimal_netcdf.nc"
    dataset = nc.Dataset(filename, "w", format="NETCDF4")

    # Create the dimensions
    dataset.createDimension("time", time)
    dataset.createDimension("lon", lon)
    dataset.createDimension("lat", lat)

    # Additional global attributes
    dataset.institution = "Your Institution"
    dataset.source = "Your Source"
    dataset.contributors = "Your Contributors"
    dataset.contact = "Your Contact"
    dataset.platform = "Your Platform"
    dataset.processid = "Your Process ID"
    dataset.project_id = "Your Project ID"
    dataset.software = "Your Software"
    dataset.title = "Your Title"

    # Create the time variable
    time_var = dataset.createVariable("time", np.float64, ("time",))
    time_var.units = "hours since 1970-01-01 00:00:00"
    time_var.calendar = "standard"
    start_time = datetime(2023, 5, 23, 0, 0, 0)
    time_values = [start_time + timedelta(hours=h) for h in range(time)]
    time_var[:] = nc.date2num(time_values, units=time_var.units, calendar=time_var.calendar)

    # Create the longitude and latitude variables
    lon_var = dataset.createVariable("lon", np.float32, ("lon",))
    lon_var.units = "degrees_east"
    lon_var[:] = [LONGITUDE_LOW, LONGITUDE_HIGH]  # Set the longitude value

    lat_var = dataset.createVariable("lat", np.float32, ("lat",))
    lat_var.units = "degrees_north"
    lat_var[:] = [LATITUDE_LOW, LATITUDE_HIGH]  # Set the latitude value

    # Create the temperature variable
    temp_var = dataset.createVariable("t2m", np.float32, ("time", "lat", "lon"))
    temp_var.units = "K"
    temp_var.long_name = "2m Temperature"
    # temp_data = np.random.uniform(low=280.0, high=310.0, size=(time, lat, lon))  # Generate random temperature values
    temp_data = np.random.uniform(low=280.0, high=310.0, size=(time, lat, lon))  # Generate random temperature values
    temp_var[:] = temp_data

    # Close the netCDF file
    dataset.close()

    return filename
