import pytest
from pathlib import Path
import xarray as xr
from typer.testing import CliRunner
from plotdata.utils import select_coordinates
from .minimal_netcdf_values import VALUES_OVER_LOCATION_0_0 
from .minimal_netcdf_values import VALUES_OVER_LOCATION_0_1 
from .minimal_netcdf_values import VALUES_OVER_LOCATION_1_0 
from .minimal_netcdf_values import VALUES_OVER_LOCATION_1_1


runner = CliRunner()


@pytest.mark.parametrize(
        'longitude, latitude, values_over_location',
        [
            (0, 0, VALUES_OVER_LOCATION_0_0),
            (0, 1, VALUES_OVER_LOCATION_0_1),
            (1, 0, VALUES_OVER_LOCATION_1_0),
            (1, 1, VALUES_OVER_LOCATION_1_1)
            ]
        )
def test_select_coordinates_with_minimal_netcdf(
    create_minimal_netcdf: Path,
    longitude: float,
    latitude: float,
    values_over_location: float or List[float]
    ) -> None:

    # Read the minimal netCDF file using xarray
    data_array = xr.open_dataset(create_minimal_netcdf)

    # Test case: Selecting center coordinates without providing longitude and latitude
    result_1 = select_coordinates(data_array)
    expected_longitude = data_array['lon'][len(data_array['lon']) // 2].values
    expected_latitude = data_array['lat'][len(data_array['lat']) // 2].values
    assert result_1.coords['lon'].values == expected_longitude
    assert result_1.coords['lat'].values == expected_latitude

    # Test case: Selecting specified coordinates without time
    result_2 = select_coordinates(data_array, longitude=longitude, latitude=latitude)
    assert result_2 == values_over_location

    # # Test case: Selecting specified coordinates with time and tolerance
    time = data_array['time'][0].values
    # tolerance = 0.2
    # result_3 = select_coordinates(data_array, longitude=longitude, latitude=latitude, time=time, tolerance=tolerance)
    result_3 = select_coordinates(data_array, longitude=longitude, latitude=latitude, time=time)
    assert result_3 == values_over_location[0]
