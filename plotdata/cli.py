import typer
from typing import Annotated
from typing import Optional
from rich import print

import sys
from .log import logger

from timeit import default_timer as timer
import warnings

from pathlib import Path
from .series import app as series
from .outliers import app as outliers


app = typer.Typer()
app.add_typer(series, name='series')
app.add_typer(outliers, name='outliers')


def _version_callback(value: bool) -> None:
    if value:
        typer.echo(f"plot-data version: {version('plot-data')}")
        raise typer.Exit(code=0)


@app.callback(no_args_is_help=True)
def main(
        verbose: Annotated[bool, typer.Option(
            help='Verbosity level',
            rich_help_panel="Options")] = False,
        version: Optional[bool] = typer.Option(
            None,
            "--version",
            "-v",
            help="Show the application's version and exit.",
            callback=_version_callback,
            is_eager=True,
            )
        ) -> None:
    """
    callback() : Plot time series
    """
    # Logging configuration
    warnings.filterwarnings("ignore")
    logger.propagate = False
    if verbose:
        print("Will write verbose output")
        state["verbose"] = True
    return


if __name__ == '__main__':
    cluster = LocalCluster(dashboard_address='localhost:8585')
    client = Client(silence_logs=logging.ERROR, processes=False)
    # client = Client(cluster)
    with client:
        start = timer()
        logger.info(f'--%<--- Start IQR --%<--- {start}')

        # sys.exit(main())
        # main()
        app()

        end = timer()
        logger.info(f'Processing time : {end-start}')
        logger.info(f'--->%--  End IQR --->%-- {end}\n')
        sys.exit()
