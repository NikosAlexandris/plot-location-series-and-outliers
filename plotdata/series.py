import typer
from typing_extensions import Annotated
from typing import Optional
from typing import Tuple
from colorama import Fore, Style
from datetime import datetime
from pathlib import Path
import xarray as xr
import numpy as np
from distributed import LocalCluster, Client
import dask

import logging
from .log import logger
import warnings
from .utils import get_scale_and_offset
from .utils import select_coordinates
from .plot import plot_series
from .hardcodings import exclamation_mark
from .hardcodings import check_mark
from .hardcodings import x_mark


app = typer.Typer(
    add_completion=False,
    add_help_option=True,
    help=f"Plot series",
)


@app.callback(invoke_without_command=True)
def series(
        netcdf: Annotated[Path, typer.Argument(help='Input netCDF file')],
        longitude: Annotated[float, typer.Argument(
            help='Longitude in decimal degrees ranging in',
            min=-180, max=360)] = None,
        latitude: Annotated[float, typer.Argument(
            help='Latitude in decimal degrees, south is negative',
            min=-90, max=90)] = None,
        time: Annotated[Optional[str], typer.Argument(
            help='Time of data to extract from series (note: use quotes)')] = None,
        convert_longitude_360: Annotated[bool, typer.Option(
            help='Convert range of longitude values to [0, 360]',
            rich_help_panel="Helpers")] = False,
        output_filename: Annotated[Path, typer.Option(
            help='Figure output filename',
            rich_help_panel='Options')] = 'series_in',
        variable_name_as_suffix: Annotated[bool, typer.Option(
            help='Suffix the output filename with the variable',
            rich_help_panel='Options')] = True,
        tufte_style: Annotated[bool, typer.Option(
            help='Try to mimic Edward Tufte style',
            rich_help_panel='Options')] = False,
    ):
    """
    Plot location series
    """
    if convert_longitude_360:
        longitude = longitude % 360

    if longitude < 0:
        warning = Fore.YELLOW + f'{exclamation_mark} '
        warning += f'The longitude ' + Style.RESET_ALL
        warning += Fore.RED + f'{longitude} ' + f'is negative. ' + Style.RESET_ALL
        warning += Fore.YELLOW + f'Consider using `--convert-longitude-360` if the dataset in question is such!' + Style.RESET_ALL
        # logger.warning(warning)
        typer.echo(Fore.YELLOW + warning)

    logger.handlers = []  # Remove any existing handlers
    file_handler = logging.FileHandler(f'{output_filename}_{netcdf.name}.log')
    file_handler.setLevel(logging.INFO)
    formatter = logging.Formatter("%(asctime)s, %(msecs)d; %(levelname)-8s; %(lineno)4d: %(message)s", datefmt="%I:%M:%S")
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)
    logger.info(f'Dataset : {netcdf.name}')
    logger.info(f'Path to : {netcdf.parent.absolute()}')
    scale_factor, add_offset = get_scale_and_offset(netcdf)
    logger.info(f'Scale factor : {scale_factor}, Offset : {add_offset}')

    if (longitude and latitude):
        logger.info(f'Coordinates : {longitude}, {latitude}')

    data_array = xr.open_dataarray(netcdf)
    data_array = select_coordinates(
            data_array=data_array,
            longitude=longitude,
            latitude=latitude,
            time=time,
            )

    if data_array.size == 1:
        single_value = float(data_array.values)
        warning = Fore.YELLOW + f'{exclamation_mark} The selection matches a single value : {single_value}' + Style.RESET_ALL
        logger.warning(warning)
        typer.echo(Fore.YELLOW + warning)
        return single_value

    try:
        output_filename = plot_series(
                data_array=data_array,
                time=time,
                figure_name=output_filename,
                add_offset=add_offset,
                variable_name_as_suffix=variable_name_as_suffix,
                tufte_style=tufte_style,
                )
    except Exception as exc:
        typer.echo(f"Something went wrong in plotting the data: {str(exc)}")
        raise SystemExit(33)


if __name__ == "__main__":
    app()
