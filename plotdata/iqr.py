def is_outlier_iqr(data_array, sensitivity_factor=1.5):
    """
    """
    # q1 = data_array.quantile(q='.25', dim='time')
    q1 = data_array.quantile(q='.25')
    # q3 = data_array.quantile(q='.75', dim='time')
    q3 = data_array.quantile(q='.75')
    IQR = q3 - q1
    lower_fence = q1 - sensitivity_factor*IQR
    upper_fence = q3 + sensitivity_factor*IQR
    # print(f'Fences : {lower_fence} - {upper_fence}')
    boolean_outliers = (data_array < lower_fence) | (data_array > upper_fence)
    return boolean_outliers

def get_outliers_iqr(data_array, sensitivity_factor=1.5):
    """
    """
    boolean_outliers = is_outlier_iqr(data_array, sensitivity_factor)
    outliers = data_array[boolean_outliers]
    return outliers
