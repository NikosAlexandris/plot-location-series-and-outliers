import typer
from typing_extensions import Annotated
from typing import Optional
from typing import Tuple
from colorama import Fore, Style

from pathlib import Path
import matplotlib.pyplot as plt
import xarray as xr
import numpy as np

import logging
from .log import logger
from .utils import get_scale_and_offset
from .utils import select_coordinates
from .iqr import is_outlier_iqr, get_outliers_iqr
from .plot import plot_outliers
from .hardcodings import exclamation_mark
from .hardcodings import check_mark
from .hardcodings import x_mark


app = typer.Typer(
    add_completion=False,
    add_help_option=True,
    rich_markup_mode="rich",
    help=f"Plot series",
)


@app.callback(invoke_without_command=True)
def outliers(
        netcdf: Annotated[Path, typer.Argument(help='Input netCDF file')],
        longitude: Annotated[float, typer.Argument(
            help='Longitude in decimal degrees ranging in [0, 360].',
            min=-180, max=360)] = None,
        latitude: Annotated[float, typer.Argument(
            help='Latitude in decimal degrees, south is negative',
            min=-90, max=90)] = None,
        sensitivity_factor: Annotated[float, typer.Option(
            help='Sensitivity factor')] = 1,
        time: Annotated[Optional[str], typer.Argument(
            help='Time of data to extract from series (note: use quotes)')] = None,
        convert_longitude_360: Annotated[bool, typer.Option(
            help='Convert range of longitude values to [0, 360]',
            rich_help_panel="Helpers")] = False,
        output_filename: Annotated[Path, typer.Option(
            help='Figure output filename',
            rich_help_panel='Options')] = 'outliers',
        variable_name_as_suffix: Annotated[bool, typer.Option(
            help='Suffix the output filename with the variable',
            rich_help_panel='Options')] = True,
        ):
    """
    Plot outliers in location series
    """
    if convert_longitude_360:
        longitude = longitude % 360

    if longitude < 0:
        warning = Fore.YELLOW + f'{exclamation_mark} '
        warning += f'The longitude ' + Style.RESET_ALL
        warning += Fore.RED + f'{longitude} ' + f'is negative. ' + Style.RESET_ALL
        warning += Fore.YELLOW + f'Consider using `--convert-longitude-360` if the dataset in question is such!' + Style.RESET_ALL
        # logger.warning(warning)
        typer.echo(Fore.YELLOW + warning)

    logger.handlers = []  # Remove any existing handlers
    file_handler = logging.FileHandler(f'{output_filename}_{netcdf.name}.log')
    file_handler.setLevel(logging.INFO)
    formatter = logging.Formatter("%(asctime)s, %(msecs)d; %(levelname)-8s; %(lineno)4d: %(message)s", datefmt="%I:%M:%S")
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)

    logger.info(f'Dataset  : {netcdf.name}')
    logger.info(f'Path to  : {netcdf.parent.absolute()}')
    # logger.info(f'Variable : {data_variable}')

    scale_factor, add_offset = get_scale_and_offset(netcdf)
    logger.info(f'Scale factor : {scale_factor}, Offset : {add_offset}')

    data_array = xr.open_dataarray(netcdf)
    first_year = data_array.time[0].values.astype('datetime64[Y]').astype(int) + 1970
    last_year = data_array.time[-1].values.astype('datetime64[Y]').astype(int) + 1970

    data_array = select_coordinates(
            data_array=data_array,
            longitude=longitude,
            latitude=latitude,
            )
    typer.echo(Fore.YELLOW + f'{exclamation_mark} Sensitivity : {sensitivity_factor}'+ Style.RESET_ALL)
    outliers = get_outliers_iqr(
            data_array,
            sensitivity_factor=sensitivity_factor
            )
    outliers_values = np.unique(outliers.values[~np.isnan(outliers.values)])

    if outliers.any():
        logger.info(Fore.RED + f'Sensitivity : {sensitivity_factor}'+ Style.RESET_ALL)
        warning = Fore.RED + f'{exclamation_mark} Outliers detected : {outliers_values}' + Style.RESET_ALL
        logger.warning(warning)
        typer.echo(warning)
        if any(outliers_values == add_offset):
            warning = Fore.RED + f'{x_mark} Outlier/s equal/s to offset : {float(outliers_values)} == {add_offset}' + Style.RESET_ALL
            logger.warning(warning)
            typer.echo(Fore.RED + warning)

        plot_outliers(
            data_array=data_array,
            time=time,
            outliers=outliers,
            sensitivity_factor=sensitivity_factor,
            figure_name=output_filename,
            add_offset=add_offset,
            variable_name_as_suffix=variable_name_as_suffix,
            )

    else:
        logger.info(Fore.GREEN + f'Sensitivity : {sensitivity_factor}'+ Style.RESET_ALL)
        logger.info(Fore.GREEN + f'{check_mark} No outliers detected!' + Style.RESET_ALL)
        print(Fore.GREEN + check_mark + ' No outliers detected!')


if __name__ == "__main__":
    app()
